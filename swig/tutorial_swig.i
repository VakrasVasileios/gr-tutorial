/* -*- c++ -*- */

#define TUTORIAL_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "tutorial_swig_doc.i"

%{
#include "tutorial/complex_clamp.h"
#include "tutorial/interleaver.h"
#include "tutorial/deinterleaver.h"
#include "tutorial/fec_encoder.h"
#include "tutorial/fec_decoder.h"
#include "tutorial/frame_sync.h"
#include "tutorial/framer.h"
%}

%include "tutorial/complex_clamp.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, complex_clamp);
%include "tutorial/interleaver.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, interleaver);
%include "tutorial/deinterleaver.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, deinterleaver);
%include "tutorial/fec_encoder.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, fec_encoder);
%include "tutorial/fec_decoder.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, fec_decoder);
%include "tutorial/frame_sync.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, frame_sync);
%include "tutorial/framer.h"
GR_SWIG_BLOCK_MAGIC2(tutorial, framer);
