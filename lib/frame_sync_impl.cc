/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "frame_sync_impl.h"

namespace gr {
namespace tutorial {

frame_sync::sptr
frame_sync::make(uint8_t preamble, uint8_t preamble_len,
                 const std::vector<uint8_t> &sync_word,
                 int mod)
{
    return gnuradio::get_initial_sptr
           (new frame_sync_impl(preamble, preamble_len, sync_word, mod));
}


/*
 * The private constructor
 */
frame_sync_impl::frame_sync_impl(uint8_t preamble, uint8_t preamble_len,
                                 const std::vector<uint8_t> &sync_word,
                                 int mod)
    : gr::sync_block("frame_sync",
                     gr::io_signature::make(1, 1, sizeof(uint8_t)),
                     gr::io_signature::make(0, 0, 0)),
      d_mod((mod_t)mod), d_preamble(preamble), d_preamble_len(preamble_len),
      d_sync_word(sync_word)
{   /*
    for (int i = 0; i < 8; i++) {
        d_preamble_reg.push_back((d_preamble>>(7-i))&1);
    }

    for (int j = 0; j < d_sync_word.size(); j++) {
        d_sync_word_reg.push_back(tutorial::shift_reg(8));
        for (int i = 0; i < 8; i++) {
            d_sync_word_reg[j].push_back((d_sync_word[j]>>(7-i))&1);
        }
    }*/
    message_port_register_out(pmt::mp("pdu"));
}

/*
 * Our virtual destructor.
 */
frame_sync_impl::~frame_sync_impl()
{
}

/*
 * Checks if reg1 is equal to reg2
 *//*
bool
are_equal(const tutorial::shift_reg reg1, const tutorial::shift_reg reg2) {
    if (reg1.size() != reg2.size()) {
        std::cout << "Register sizes differ, reg1.size: " << reg1.size() << ", reg2.size: " << reg2.size() << std::endl;
        return false;
    }//std::cout << reg1 << reg2 << std::endl;
    for (size_t i = 0; i < reg1.size(); i++) {
        if (reg1[i] != reg2[i]) {
            return false;
        }
    }
    return true;
}*/
/*
 * Checks if reg is equal to word within an error margine given as a percentage
 *//*
bool
are_equal_with_error_margin(const tutorial::shift_reg reg, const tutorial::shift_reg word, float err_marg) {
    if (reg.len() != word.len()) {
        std::cout << "Register sizes differ, reg.size: " << reg.size() << ", word.size: " << word.size() << std::endl;
        return false;
    }//std::cout << reg << word << std::endl;
    if (!are_equal(reg, word))
    {//std::cout << "checking for error margin" << std::endl;
        if (const_cast<tutorial::shift_reg&>(reg).count()
                >= (const_cast<tutorial::shift_reg&>(word).count() * (size_t)(1-err_marg)))
        {//std::cout << "Within error margin" << std::endl;
            return true;
        }
        else
            return false;
    }
    return true;
}*/
/*
uint8_t
turn_to_byte(const tutorial::shift_reg reg) {
    if (reg.size() > 8) {
        std::cout << "Register size larger than 1-byte." << std::endl;
        return 0;
    }
    uint8_t ret = 0;
    for (int i = 0; i < 8; i++) {
        ret = ret | reg[i];
        if (i < 7) ret = ret << 1;
    }

    return ret;
}*/

uint8_t
frame_sync_impl::get_byte() {
    uint8_t ret = 0;
    for (int i = 0; i < 8; i++){
        uint8_t tmp = *buffer_iter;
        ret |= tmp << (7-(i%8));
        ++buffer_iter;
        ++index;
    }

    return ret;
}
// Flushes bytes, until a non-preamble byte is found which is then returned
uint8_t
frame_sync_impl::trash_preamble() {
    uint8_t search = get_byte();
    while (search == d_preamble) {
        search = get_byte();
    }
    return search;
}

uint16_t
frame_sync_impl::show_pdu_len() {
    uint16_t ret = 0;
    for (int i = 0; i < 16; i++) {
        ret = (ret << 1) | *buffer_iter;
        ++buffer_iter;
    }
    std::advance(buffer_iter, -16);
    return ret;
} 

#define HEX(x)  std::hex << (unsigned int)x
int
frame_sync_impl::work(int noutput_items,
                      gr_vector_const_void_star &input_items,
                      gr_vector_void_star &output_items)
{
    const uint8_t *in = (const uint8_t *) input_items[0];
    for (size_t t = 0; t < noutput_items; t++)
        buffer.push_back(in[t]);

    // Do <+signal processing+>
    /*
     * GNU Radio handles PMT messages in a pair structure.
     * The first element corresonds to possible metadata (in our case we do not
     * have assosiated metadata) whereas the second element contains the
     * data in a vector of uint8_t.
     *
     * When the frame is extracted, you will possible hold it in a *uint8_t buffer,
     * To create a PMT pair message you can use:
     * pmt::pmt_t pair = pmt::cons(pmt::PMT_NIL, pmt::init_u8vector(frame_len, buf));
     *
     * Then you can send this message to the port we have registered at the contructpr
     * using:
     *
     * message_port_pub(pmt::mp("pdu"), pair);
     */
    
    uint16_t data_size = 0;
    size_t sync_len = d_preamble_len+d_sync_word.size();
    int sync_words_found = 0;
    pmt::pmt_t pdu;
    while (!buffer.empty()/* && buffer_iter != buffer.end()*/) {
        uint8_t search = get_byte();
        while (search != d_preamble) {
            search = *buffer_iter | (search << 1);
            ++buffer_iter;
        }
        if (trash_preamble() == d_sync_word[sync_words_found]) {
            ++sync_words_found;
            for (int i = 0; i < d_sync_word.size()-1; i++) {
                if (get_byte() != d_sync_word[sync_words_found]) {
                    sync_words_found = 0;
                    break;
                }
                else 
                    ++sync_words_found;
            }
        }
        // check pdu integrity
        if ((buffer.size()-index+1)/8 < show_pdu_len()) {
            std::advance(buffer_iter, -(1+d_sync_word.size())*8);
            buffer.erase(buffer.begin(), buffer_iter);
            
            return noutput_items;
        }
        data_size = get_byte();
        data_size = get_byte() | (data_size << 8);
        pdu = pmt::make_u8vector(data_size, 0);
        for (int i = 0; i < data_size; i++) {
            pmt::u8vector_set(pdu, i, get_byte());
        }
        message_port_pub(pmt::mp("pdu"), pmt::cons(pmt::PMT_NIL, pdu));
        pdu.reset();
        index = 0;
        //buffer.erase(buffer.begin(), buffer_iter);
        //buffer_iter=buffer.begin();
        return noutput_items;
    }

    // Tell runtime system how many output items we produced.
    buffer.clear();
    pdu.reset();
    index = 0;
    buffer_iter=buffer.begin();
    return noutput_items;
}



} /* namespace tutorial */
} /* namespace gr */


