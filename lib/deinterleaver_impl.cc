/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "deinterleaver_impl.h"

namespace gr {
namespace tutorial {

deinterleaver::sptr
deinterleaver::make(size_t block_size)
{
    return gnuradio::get_initial_sptr
           (new deinterleaver_impl(block_size));
}

/*
 * The private constructor
 */
deinterleaver_impl::deinterleaver_impl(size_t block_size)
    : gr::block("deinterleaver",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
                d_block_size(block_size)
{
    message_port_register_in(pmt::mp("pdu_in"));
    message_port_register_out(pmt::mp("pdu_out"));

    /* Register the message handler. For every message received in the input
     * message port it will be called automatically.
     */
    set_msg_handler(pmt::mp("pdu_in"),
    [this](pmt::pmt_t msg) {
        this->deinterleaver_impl::deinterleave(msg);
    });
}

bool
deinterleaver_impl::get_front() {
    bool ret;
    if (!bit_buffer.empty()) {
        ret = bit_buffer.front();
        bit_buffer.pop_front();
        return ret;
    }
    else
        return false;
}

uint8_t
deinterleaver_impl::get_byte() {
    uint8_t ret = 0;
    for (int i = 0; i < 8; i++){
        ret = ret << 1;
        ret |= bit_buffer.front();
        bit_buffer.pop_front();
    }

    return ret;
}

/*
 * Our virtual destructor.
 */
deinterleaver_impl::~deinterleaver_impl()
{
}

void
deinterleaver_impl::deinterleave(pmt::pmt_t m)
{
    /* TODO: Add your code here */

    pmt::pmt_t meta(pmt::car(m));
    pmt::pmt_t bytes(pmt::cdr(m));
    size_t pdu_len;
    const uint8_t *bytes_in = pmt::u8vector_elements(bytes, pdu_len);
    size_t rows_needed = (pdu_len*8) / 96 + (1 * ((pdu_len*8) % 96 != 0));
    bool** block = new bool*[d_block_size];
    size_t pair_len = (d_block_size*rows_needed) / 8;
    pmt::pmt_t pair = pmt::make_u8vector(pair_len, 0);

    // break to bits
    for (int i = 0; i < pdu_len; i++) {
        bit_buffer.push_back((bytes_in[i] >> 7) & 0x01);
        bit_buffer.push_back((bytes_in[i] >> 6) & 0x01);
        bit_buffer.push_back((bytes_in[i] >> 5) & 0x01);
        bit_buffer.push_back((bytes_in[i] >> 4) & 0x01);
        bit_buffer.push_back((bytes_in[i] >> 3) & 0x01);
        bit_buffer.push_back((bytes_in[i] >> 2) & 0x01);
        bit_buffer.push_back((bytes_in[i] >> 1) & 0x01);
        bit_buffer.push_back((bytes_in[i]) & 0x01);
    }
    // fill array row wise
    for (int row = 0; row < d_block_size; row++) {
        block[row] = new bool[rows_needed];
        for (int col = 0; col < rows_needed; col++) {
            block[row][col] = get_front();
        }
    }
    // read column wise
    for (int col = 0; col < rows_needed; col++) {
        for (int row = 0; row < d_block_size; row++) {
            bit_buffer.push_back(block[row][col]);
        }
    }
    // send bytes
    for (size_t i = 0; !bit_buffer.empty(); ) {
        pmt::u8vector_set(pair, i++, get_byte());
    }

    /*
     * FIXME: This just copies the input to the output. Even you do not
     * implement the deinterleaver, it will forward the input message to the next
     * block and everything should work fine
     */
    message_port_pub(pmt::mp("pdu_out"), pmt::cons(pmt::PMT_NIL, pair));
}


} /* namespace tutorial */
} /* namespace gr */

