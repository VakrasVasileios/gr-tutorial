/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "fec_decoder_impl.h"

namespace gr {
namespace tutorial {

fec_decoder::sptr
fec_decoder::make(int type)
{
    return gnuradio::get_initial_sptr
           (new fec_decoder_impl(type));
}


/*
 * The private constructor
 */
fec_decoder_impl::fec_decoder_impl(int type)
    : gr::block("fec_encoder",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      d_type(type)
{
    message_port_register_in(pmt::mp("pdu_in"));
    message_port_register_out(pmt::mp("pdu_out"));

    /* Register the message handler. For every message received in the input
     * message port it will be called automatically.
     */
    set_msg_handler(pmt::mp("pdu_in"),
    [this](pmt::pmt_t msg) {
        this->fec_decoder_impl::decode(msg);
    });
}

/*
 * Our virtual destructor.
 */
fec_decoder_impl::~fec_decoder_impl()
{
}

uint8_t
fec_decoder_impl::deduct_byte() {
    uint8_t ret = 0;
    int reg = 0;
    for (int i = 0; i < 8; i++) {
        reg |= bit_buffer.front(); bit_buffer.pop_front();
        reg = (reg << 1) | bit_buffer.front(); bit_buffer.pop_front();
        reg = (reg << 1) | bit_buffer.front(); bit_buffer.pop_front();
        
        ret = ret << 1;
        ret |= d_ref_table[reg];
        reg = 0;
    }

    return ret;
}

void
fec_decoder_impl::decode(pmt::pmt_t m)
{
    pmt::pmt_t meta(pmt::car(m));
    pmt::pmt_t bytes(pmt::cdr(m));
    size_t pdu_len;
    const uint8_t *bytes_in = pmt::u8vector_elements(bytes, pdu_len);
    int reg = 0;
    pmt::pmt_t pair = pmt::make_u8vector(pdu_len/3, 0);
    switch (d_type) {
    /* No FEC just copy the input message to the output */
    case 0:
        message_port_pub(pmt::mp("pdu_out"), m);
        return;
    case 1:
        /* Do Hamming encoding */
        for (int i = 0; i < pdu_len; i++) {
            bit_buffer.push_back((bytes_in[i] >> 7) & 0x01);
            bit_buffer.push_back((bytes_in[i] >> 6) & 0x01);
            bit_buffer.push_back((bytes_in[i] >> 5) & 0x01);
            bit_buffer.push_back((bytes_in[i] >> 4) & 0x01);
            bit_buffer.push_back((bytes_in[i] >> 3) & 0x01);
            bit_buffer.push_back((bytes_in[i] >> 2) & 0x01);
            bit_buffer.push_back((bytes_in[i] >> 1) & 0x01);
            bit_buffer.push_back((bytes_in[i]) & 0x01);
        }
        for(size_t i = 0; !bit_buffer.empty(); ) {
            pmt::u8vector_set(pair, i++, deduct_byte());
        }
        message_port_pub(pmt::mp("pdu_out"), pmt::cons(pmt::PMT_NIL, pair));
        return;
    case 2:
        /* Do Golay encoding */
        return;
    default:
        throw std::runtime_error("fec_decoder: Invalid FEC");
        return;
    }
}

} /* namespace tutorial */
} /* namespace gr */

