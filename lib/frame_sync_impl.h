/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_TUTORIAL_FRAME_SYNC_IMPL_H
#define INCLUDED_TUTORIAL_FRAME_SYNC_IMPL_H

#include <tutorial/frame_sync.h>
#include <tutorial/shift_reg.h>

namespace gr {
namespace tutorial {

class frame_sync_impl : public frame_sync {
private:
    typedef enum {
        BPSK,
        QPSK
    } mod_t;
    std::list<bool> buffer;
    std::list<bool>::iterator buffer_iter = buffer.begin();
    size_t index = 0;
    uint8_t get_byte();
    uint8_t trash_preamble();
    uint16_t show_pdu_len();
    const mod_t d_mod;
    uint8_t d_preamble;
    uint8_t d_preamble_len;
    std::vector<uint8_t> d_sync_word;

public:
    frame_sync_impl(uint8_t preamble, uint8_t preamble_len,
                    const std::vector<uint8_t> &sync_word,
                    int mod);
    ~frame_sync_impl();

    // Where all the action really happens
    int work(
        int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items
    );
};

} // namespace tutorial
} // namespace gr

#endif /* INCLUDED_TUTORIAL_FRAME_SYNC_IMPL_H */

