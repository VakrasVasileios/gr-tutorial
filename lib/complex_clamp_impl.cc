/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "complex_clamp_impl.h"

namespace gr {
namespace tutorial {

complex_clamp::sptr
complex_clamp::make(float threshold)
{
    return gnuradio::get_initial_sptr
           (new complex_clamp_impl(threshold));
}


/*
 * The private constructor
 */
complex_clamp_impl::complex_clamp_impl(float threshold)
    : gr::sync_block("complex_clamp",
                     /* Exactly one complex input */
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     /* Exactly one complex output */
                     gr::io_signature::make(1, 1, sizeof(gr_complex))),
      /* Initialize private members */
      d_threshold(threshold)
{

}

/*
 * Our virtual destructor.
 */
complex_clamp_impl::~complex_clamp_impl()
{
}

int
complex_clamp_impl::work(int noutput_items,
                         gr_vector_const_void_star &input_items,
                         gr_vector_void_star &output_items)
{
    /*Get the input items. NOTE: No modification is allowed on them*/
    const gr_complex *in = (const gr_complex *) input_items[0];
    gr_complex *out = (gr_complex *) output_items[0];


    // Do <+signal processing+>
    for (int i = 0; i < noutput_items; i++) {
        out[i] = in[i];
        if (in[i].real() > d_threshold) {
            out[i].real(d_threshold);
        }
        if (in[i].imag() > d_threshold) {
            out[i].imag(d_threshold);
        }
        if (in[i].real() < -d_threshold) {
            out[i].real(-d_threshold);
        }
        if (in[i].imag() < -d_threshold) {
            out[i].imag(-d_threshold);
        }
    }
    // Tell runtime system how many output items we produced.
    return noutput_items;

}

} /* namespace tutorial */
} /* namespace gr */

