/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_TUTORIAL_FEC_DECODER_IMPL_H
#define INCLUDED_TUTORIAL_FEC_DECODER_IMPL_H

#include <tutorial/fec_decoder.h>
#include <tutorial/shift_reg.h>

namespace gr {
namespace tutorial {

class fec_decoder_impl : public fec_decoder {
private:
    const int d_type;
    std::list<bool> bit_buffer;
    const uint8_t d_ref_table[8] = { 0, 0, 0, 1, 0, 1, 1, 1 };

    uint8_t deduct_byte();

    void decode(pmt::pmt_t m);

public:
    fec_decoder_impl(int type);
    ~fec_decoder_impl();

};

} // namespace tutorial
} // namespace gr

#endif /* INCLUDED_TUTORIAL_FEC_DECODER_IMPL_H */

