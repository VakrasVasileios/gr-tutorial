/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_TUTORIAL_FRAMER_H
#define INCLUDED_TUTORIAL_FRAMER_H

#include <tutorial/api.h>
#include <gnuradio/block.h>

namespace gr {
namespace tutorial {

/*!
 * \brief Constructs a frame with the received payload
 *
 * The frame has the following format
 * ------------------------------------------------------------------------
 * | preamble  | sync word | frame length (uint16_t) | payload            |
 * ------------------------------------------------------------------------
 *
 * \ingroup tutorial
 *
 */
class TUTORIAL_API framer : virtual public gr::block {
public:
    typedef boost::shared_ptr<framer> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of tutorial::framer.
     *
     * To avoid accidental use of raw pointers, tutorial::framer's
     * constructor is in a private implementation
     * class. tutorial::framer::make is the public interface for
     * creating new instances.
     */
    static sptr make(uint8_t preamble, size_t preamble_len,
                     const std::vector<uint8_t> &sync_word);
};

} // namespace tutorial
} // namespace gr

#endif /* INCLUDED_TUTORIAL_FRAMER_H */

